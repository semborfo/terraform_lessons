provider "aws" {
  region     = "eu-central-1"
}

data "aws_ami" "latest_ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

output "latest_ubuntu_ami_id" {
  value = data.aws_ami.latest_ubuntu.id
}

output "latest_ubuntu_ami_name" {
  value = data.aws_ami.latest_ubuntu.name
}
# resource "aws_instance" "my_Linux" {
#   ami           = "ami-043097594a7df80ec"
#   instance_type = "t3.small"
#   tags = {
#     Name    = "My Amazon Server"
#     Ownes   = "Fokin Simon"
#     Project = "Terraform Lessons"
#   }
# }
# resource "aws_instance" "my_Linux" {
#   ami           = "ami-043097594a7df80ec"
#   instance_type = "t3.small"
#   tags = {
#     Name    = "My Amazon Server"
#     Ownes   = "Fokin Simon"
#     Project = "Terraform Lessons"
#   }
# }
