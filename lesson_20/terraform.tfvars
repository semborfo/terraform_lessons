region = "ca-central-1"
instance_type = "t3.micro"
allow_ports = ["80", "443"]
detailed_monitoring = "false"
common-tags = {

  Owner = "Semen Fokin"
  Project = "Phoenix"
  Environment = "dev"
  }
