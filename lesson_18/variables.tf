variable "region" {
  description = "Enter region:"
  default     = "ca-central-1"
}
variable "instance_type" {
  default = "t3.micro"
}
variable "allow_ports" {
  type    = list(any)
  default = ["80", "443"]
}
variable "detailed_monitoring" {
  type    = bool
  default = "true"
}
variable "common-tags" {
  type = map(any)
  default = {
    Owner = "Semen Fokin"
  }
}
