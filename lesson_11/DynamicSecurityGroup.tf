#------------------------------------------------------------------
# My terraform#
#
# Build WebServer during Bootsrap
#
# Mede by Simon
#------------------------------------------------------------------

provider "aws" {
  region     = "eu-central-1"
}

resource "aws_security_group" "my_web_server" {
  name        = "Dynamic Security Group"
  description = "My First Security Group"

  dynamic "ingress" {
    for_each = ["80", "443", "8080"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name  = "Security Group First Built"
    Owner = "SimonFB"
  }
}
