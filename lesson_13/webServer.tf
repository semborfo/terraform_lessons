#------------------------------------------------------------------
# My terraform#
#
# Build WebServer during Bootsrap
#
# Mede by Simon
#------------------------------------------------------------------

provider "aws" {
  region     = "eu-central-1"
}

resource "aws_instance" "my_server_web" {
  ami                    = "ami-043097594a7df80ec" # Amazon Linux AMI
  instance_type          = "t3.micro"
  vpc_security_group_ids = [aws_security_group.my_web_server.id]
  tags = {
    Name = "Web"
  }
  depends_on = [aws_instance.my_server_db, aws_instance.my_server_app]
}
resource "aws_instance" "my_server_app" {
  ami                    = "ami-043097594a7df80ec" # Amazon Linux AMI
  instance_type          = "t3.micro"
  vpc_security_group_ids = [aws_security_group.my_web_server.id]
  tags = {
    Name = "App"
  }
  depends_on = [aws_instance.my_server_db]
}
resource "aws_instance" "my_server_db" {
  ami                    = "ami-043097594a7df80ec" # Amazon Linux AMI
  instance_type          = "t3.micro"
  vpc_security_group_ids = [aws_security_group.my_web_server.id]
  tags = {
    Name = "Db"
  }
}


resource "aws_security_group" "my_web_server" {
  name        = "WebServer Security Group"
  description = "My First Security Group"

  dynamic "ingress" {
    for_each = ["80", "443", "22"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name  = "Security Group First Built"
    Owner = "SimonFB"
  }
}
