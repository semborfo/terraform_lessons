#------------------------------------------------------------------
# My terraform#
#
# Build WebServer during Bootsrap
#
# Mede by Simon
#------------------------------------------------------------------

provider "aws" {
  region     = "eu-central-1"
}

resource "aws_eip" "my_static_ip" {
  instance = aws_instance.my_webserver.id
}

resource "aws_instance" "my_webserver" {
  ami                    = "ami-043097594a7df80ec" # Amazon Linux AMI
  instance_type          = "t3.micro"
  vpc_security_group_ids = [aws_security_group.my_web_server.id]
  user_data = templatefile("user_data.sh.tpl", {
    f_name = "Simon"
    l_name = "Fokin"
    names  = ["Vika", "Katya", "Zelda"]
  })
  tags = {
    Name  = "Web Server First Built"
    Owner = "SimonFB"
  }
  lifecycle {
    # prevent_destroy = true
    # ignore_changes = ["user_data"]
    create_before_destroy = true
  }
}

resource "aws_security_group" "my_web_server" {
  name        = "WebServer Security Group"
  description = "My First Security Group"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name  = "Security Group First Built"
    Owner = "SimonFB"
  }
}
